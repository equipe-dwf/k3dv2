import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {AlertController, Content, NavController} from '@ionic/angular';
import {TabsPage} from '../tabs/tabs.page';
import {AuthService} from '../services/auth/auth.service';
import {MessageService} from '../services/message/message.service';

@Component({
    selector: 'app-message',
    templateUrl: './message.page.html',
    styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit {
    messages: Object;
    message: any;
    subscriberMessages: Subscription;
    @ViewChild(Content) content: Content;


    constructor(public navCtrl: NavController,
                public messageService: MessageService,
                public auth: AuthService,
                public alertCtrl: AlertController) {
    }

    ngOnInit(): void {
        this.subscriberMessages = this.messageService.getMessages().subscribe(data => this.messages = data);
        this.scrollToBottom();
    }

    OnDestroy(): void {
        this.subscriberMessages.unsubscribe();
        console.log('destroy');
    }

    lookNotification(userId, requestId) {
    }

    async newMessage(message): Promise<void> {
        await this.messageService.newMessage(message);
        this.message = '';
    }

    async errorAlert(): Promise<void> {
        const alert = await this.alertCtrl.create({
            header: 'Accès impossible',
            subHeader: 'Vous ne pouvez pas accéder a la notification, car vous n\'êtes pas le propriétaire',
            buttons: ['Annuler']
        });
        await alert.present();
    }


    async logout(): Promise<void> {
        this.subscriberMessages.unsubscribe();
        // await this.tabs.logout();
        await this.navCtrl.goRoot('/login');
    }

    scrollToBottom() {
        /*const dimensions = this.content.fullscreen();
        this.content.scrollTo(0, dimensions.contentHeight + 100000000);*/
    }

}
