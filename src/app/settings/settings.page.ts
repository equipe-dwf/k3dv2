import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {NativeGeocoderService} from '../services/native-geocoder/native-geocoder.service';
import {SettingsService} from '../services/settings/settings.service';
import {UserInfoService} from '../services/user-info/user-info.service';


@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit, OnDestroy {
    settings: any;
    addressGeolocation = 'Aucune Adresse';
    addressPrelocation = 'Aucune Adresse';
    subscriberSettings: Subscription;
    version = '2.0.0';

    constructor(public userInfo: UserInfoService,
                public settingService: SettingsService,
                public nativeGeocoder: NativeGeocoderService) {
    }

    ngOnInit() {
        this.subscriberSettings = this.userInfo.getInfos().subscribe(async (data) => {
            this.settings = data;
            await Promise.all([
                this.getAddressPrelocation(this.settings.location.latitudePrelocation, this.settings.location.longitudePrelocation),
                this.getAddressGeolocation(
                    this.settings.location.latitude, this.settings.location.longitude, this.settings.settings.geolocation
                )
            ]);
        });
    }

    ngOnDestroy() {
        console.log('destroy');
        this.subscriberSettings.unsubscribe();
    }

    async getAddressPrelocation(latitude, longitude): Promise<void> {
        if (latitude && longitude) {
            this.addressPrelocation = await this.nativeGeocoder.getAddress(parseFloat(latitude), parseFloat(longitude));
        }
    }

    async getAddressGeolocation(latitude, longitude, geolocation: boolean): Promise<void> {
        if (!latitude && !longitude && geolocation) {
            this.addressGeolocation = 'en cours de géolocalisation';
        } else if (latitude && longitude) {
            this.addressGeolocation = await this.nativeGeocoder.getAddress(parseFloat(latitude), parseFloat(longitude));
        } else {
            this.addressGeolocation = 'Géolocalisation désactivée';
        }
    }

    async updateGeolocation(value): Promise<void> {
        await this.settingService.updateGeolocation(value);
    }

    async updatePrelocation(value): Promise<void> {
        await this.settingService.updatePrelocation(value);
    }
}
