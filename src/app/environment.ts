export const firebaseConfig = {
    apiKey: 'AIzaSyAX95ZDpqsk5GCXtytQ-SfDk-TcKwU8vs8',
    authDomain: 'application-k3d.firebaseapp.com',
    databaseURL: 'https://application-k3d.firebaseio.com',
    projectId: 'application-k3d',
    storageBucket: 'application-k3d.appspot.com',
    messagingSenderId: '611612276521'
};

export const firebaseConfigTest = {
    apiKey: 'AIzaSyAlHfTgGqMzfG4R_-9-PUX4-VeeXDqRkCY',
    authDomain: 'application-k3d-test.firebaseapp.com',
    databaseURL: 'https://application-k3d-test.firebaseio.com',
    projectId: 'application-k3d-test',
    storageBucket: 'application-k3d-test.appspot.com',
    messagingSenderId: '678076279813'
};
