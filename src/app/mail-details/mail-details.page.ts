import {Component, OnInit} from '@angular/core';
import {MailService} from '../services/mail/mail.service';
import {Subscription} from 'rxjs/index';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-mail-details',
    templateUrl: './mail-details.page.html',
    styleUrls: ['./mail-details.page.scss'],
})
export class MailDetailsPage implements OnInit {
    mail: any;
    subscriberMail: Subscription;
    id: string;

    constructor(private route: ActivatedRoute,  public mailService: MailService) {
        this.id = this.route.snapshot.paramMap.get('id');

    }

    ngOnInit() {
        this.subscriberMail = this.mailService.getMail(this.id).subscribe(
            mail => this.mail = mail,
            () => console.log('error'),
            () => {
                if (this.mail.lu_membre !== 'oui') {
                    this.mailService.archiveMail({'id': this.id});
                }
            }
        );
    }

    OnDestroy() {
        console.log('destroy');
        this.subscriberMail.unsubscribe();
    }
}
