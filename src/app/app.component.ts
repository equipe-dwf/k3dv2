import {Component, ViewChild} from '@angular/core';

import {Nav, NavController, Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {AuthService} from './services/auth/auth.service';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {

    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private auth: AuthService,
                public router: Router,
                private afAuth: AngularFireAuth) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.checkUser();
        });
    }

    checkUser() {
        const unsubscribe = this.afAuth.auth.onAuthStateChanged(async user => {
            if (user) {
                await this.router.navigate(['/admin']);
                unsubscribe();
            } else {
                await this.router.navigate(['/login']);
                unsubscribe();
            }
        });
    }
}

