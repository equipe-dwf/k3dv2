import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsAddressPage } from './settings-address.page';

describe('SettingsAddressPage', () => {
  let component: SettingsAddressPage;
  let fixture: ComponentFixture<SettingsAddressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsAddressPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsAddressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
