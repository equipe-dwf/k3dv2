import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import {TabsPage} from './tabs.page';
import {MessagePageModule} from '../message/message.module';
import {MailPageModule} from '../mail/mail.module';
import {MailDetailsPageModule} from '../mail-details/mail-details.module';
import {SettingsPageModule} from '../settings/settings.module';
import {SettingsAddressPageModule} from '../settings-address/settings-address.module';



@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        TabsPageRoutingModule,
        MailDetailsPageModule,
        MessagePageModule,
        MailPageModule,
        SettingsPageModule,
        SettingsAddressPageModule,
    ],
    declarations: [TabsPage]
})
export class TabsPageModule {
}
