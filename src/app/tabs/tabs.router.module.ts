import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TabsPage} from './tabs.page';
import {MessagePage} from '../message/message.page';
import {MailPage} from '../mail/mail.page';
import {MailDetailsPage} from '../mail-details/mail-details.page';
import {SettingsPage} from '../settings/settings.page';
import {SettingsAddressPage} from '../settings-address/settings-address.page';

const routes: Routes = [
    {
        path: 'tabs',
        component: TabsPage,
        children: [
            {
                path: 'message',
                outlet: 'message',
                component: MessagePage
            },
            {
                path: 'settings',
                outlet: 'settings',
                component: SettingsPage
            },
            {
                path: 'address',
                outlet: 'settings',
                component: SettingsAddressPage
            },
            {
                path: 'mail',
                outlet: 'mail',
                component: MailPage,
            },
            {
                path: 'mail/:id',
                outlet: 'mail',
                component: MailDetailsPage,
            },
        ]
    },
    {
        path: '',
        redirectTo: '/admin/tabs/(message:message)',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
