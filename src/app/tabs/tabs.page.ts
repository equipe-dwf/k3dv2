import {Component, OnInit} from '@angular/core';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.page.html',
    styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

    constructor(private storage: Storage) {
    }

    ngOnInit() {
        this.storage.set('idClient', '75');
    }

    logout() {
        console.log('logout');
    }

    ionChange() {
        console.log('change');
    }

}
