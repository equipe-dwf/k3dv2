import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/index';
import {MailService} from '../services/mail/mail.service';

@Component({
    selector: 'app-mail',
    templateUrl: './mail.page.html',
    styleUrls: ['./mail.page.scss'],
})
export class MailPage implements OnInit, OnDestroy {
    mails: any;
    subscriberMail: Subscription;

    constructor(public mailService: MailService) {
    }

    ngOnInit() {
        console.log('init');
        this.subscriberMail = this.mailService.getMails().subscribe(mails => this.mails = mails);
    }

    ngOnDestroy(): void {
        console.log('destroy');
    }

}
