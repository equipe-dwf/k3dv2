import {Component, OnInit} from '@angular/core';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';
import {AlertController, ToastController} from '@ionic/angular';
import {AuthService} from '../services/auth/auth.service';
import {Router} from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    account: { email: string, password: string } = {
        email: 'mickael.lortet@gmail.com',
        password: '123456'
    };
    email: string;
    showing = true;

    constructor(public iab: InAppBrowser,
                public toastCtrl: ToastController,
                public auth: AuthService,
                public alertCtrl: AlertController,
                public router: Router) {
    }

    async ngOnInit() {
    }

    async doLogin(email, password): Promise<void> {
        this.showing = false;
        try {
            await this.auth.emailLogin(email, password);
            await this.router.navigate(['/admin']);

        } catch (error) {
            await this.showToast('Le nom d\'utilisateur et le mot de passe que vous avez entrés ne ' +
                'correspondent pas à ceux présents dans nos fichiers. Veuillez vérifier et réessayer.');
        }
        this.showing = true;
    }


    async doPrompt() {
        const alert = await this.alertCtrl.create({
            header: 'Mot de passe perdu',
            message: 'Entrer votre email',
            inputs: [
                {
                    name: 'email',
                    type: 'email',
                    placeholder: 'email'
                },
            ],
            buttons: [
                {
                    text: 'annuler',
                    role: 'cancel',
                },
                {
                    text: 'Envoyer',
                    handler: async (data: any) => {
                        await this.lostPassword(data.email);
                    }
                }
            ]
        });

        await alert.present();
    }

    async lostPassword(email): Promise<void> {
        try {
            await this.auth.newPassword(email);
            await this.showToast('Un nouveau mot de passe a été envoyée');
        } catch (e) {
            await this.showToast('Erreur : l\'addresse Email n\'existe pas');
        }
    }

    async showToast(message: string): Promise<void> {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });
        await toast.present();
    }

    openPage(url): void {
        this.iab.create(url);
    }


}
