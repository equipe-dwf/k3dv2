import {NgModule, LOCALE_ID} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, RouteReuseStrategy, Routes} from '@angular/router';


import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

import { IonicStorageModule } from '@ionic/storage';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';

import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';

import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {firebaseConfigTest} from './environment';
import {HttpClientModule} from '@angular/common/http';

registerLocaleData(localeFr);
@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        HttpClientModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(firebaseConfigTest),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        IonicStorageModule.forRoot()
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: LOCALE_ID, useValue: 'fr'},
        InAppBrowser,
        NativeGeocoder,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
