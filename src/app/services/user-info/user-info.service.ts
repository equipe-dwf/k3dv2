import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AuthService} from '../auth/auth.service';
import {Platform} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class UserInfoService {
    date = new Date();
    url: string;
    platformClient = 'NC';

    constructor(public afDatabase: AngularFireDatabase, public auth: AuthService, public platform: Platform) {
        if (this.platform.is('ios')) {
            this.platformClient = 'ios';
        } else if (this.platform.is('android')) {
            this.platformClient = 'android';
        }
         this.url  = `users/${this.auth.currentUserId}`;
    }

    getInfos() {
        return this.afDatabase.object(this.url).valueChanges();
    }

    sendInfos() {
        return this.afDatabase.object(this.url).update({
            platform: this.platformClient,
            lastConnexion: `${this.date.toDateString()} ${this.date.toTimeString()}`
        });
    }

    updateToken(token) {
        return this.afDatabase.object(this.url).update({token: token});
    }
}
