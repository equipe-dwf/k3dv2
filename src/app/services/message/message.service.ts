import { Injectable } from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AuthService} from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
    url: string;

    constructor(public afDatabase: AngularFireDatabase, public auth: AuthService) {
        this.url = `groupes/75/messages`;
    }

    getMessages() {
        return this.afDatabase.list(this.url , ref => ref.orderByChild('date').limitToLast(1000)).valueChanges();
    }

    newMessage(message: string) {
        const data = {content: message};
        return this.afDatabase.list(this.url).push(data);
    }
}
