import { TestBed, inject } from '@angular/core/testing';

import { NativeGeocoderService } from './native-geocoder.service';

describe('NativeGeocoderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NativeGeocoderService]
    });
  });

  it('should be created', inject([NativeGeocoderService], (service: NativeGeocoderService) => {
    expect(service).toBeTruthy();
  }));
});
