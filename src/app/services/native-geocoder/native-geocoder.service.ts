import {Injectable} from '@angular/core';
import {NativeGeocoder, NativeGeocoderReverseResult} from '@ionic-native/native-geocoder/ngx';

@Injectable({
    providedIn: 'root'
})
export class NativeGeocoderService {

    constructor(public nativeGeocoder: NativeGeocoder) {
    }

    async getAddress(latitude: number, longitude: number): Promise<string> {
        const result: NativeGeocoderReverseResult[] = await this.nativeGeocoder.reverseGeocode(latitude, longitude);
        if (result[0].subThoroughfare) {
            return result[0].subThoroughfare + ' ' + result[0].thoroughfare + ', ' + result[0].postalCode + ' ' + result[0].locality;
        } else {
            return result[0].thoroughfare + ', ' + result[0].postalCode + ' ' + result[0].locality;
        }
    }

    async getLatLng(address: string) {
        return await this.nativeGeocoder.forwardGeocode(address);
    }
}
