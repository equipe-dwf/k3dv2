import {Injectable} from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root'
})
export class MailService {
    mailUrl = 'http://k3d-sarl.fr/api';

    constructor(private http: HttpClient) {
    }


    getMails(): Observable<any[]> {
        return this.http.get<any[]>(`${this.mailUrl}/read.php?idClient=${localStorage.getItem('idClient')}`);
    }

    getMail(id): Observable<any[]> {
        return this.http.get<any[]>(`${this.mailUrl}/read_one.php?id=${id}`);
    }

    archiveMail(data: any): Observable<any> {
        return this.http.put(`${this.mailUrl}/update.php`, data, httpOptions);
    }
}
