import {Injectable} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    authState: any = null;

    constructor(private afAuth: AngularFireAuth) {
        this.afAuth.auth.onAuthStateChanged((user) => {
                this.authState = user;
            }
        );
    }

    // Returns true if user is logged in
    authenticated() {
        return this.authState !== null;
    }

    // Returns current user data
    currentUser(): any {
        return this.authenticated ? this.authState : null;
    }

    // Returns
    get currentUserObservable(): any {
        return this.afAuth.authState;
    }

    // Returns current user UID
    get currentUserId(): string {
        console.log(this.authState.uid);
        return this.authenticated ? this.authState.uid : '';
    }


    async emailLogin(email: string, password: string) {
        try {
            const user = await this.afAuth.auth.signInWithEmailAndPassword(email, password);
            return this.authState = user;
        } catch (error) {
            throw error;
        }
    }

    async signOut(): Promise<void> {
        await this.afAuth.auth.signOut();
        this.authState = null;
    }

    async newPassword(email) {
        try {
            return await this.afAuth.auth.sendPasswordResetEmail(email);
        } catch (error) {
            throw error;
        }
    }

}
