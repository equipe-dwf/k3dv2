import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AuthService} from '../auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {
    url: string;

    constructor(public afDatabase: AngularFireDatabase, public auth: AuthService) {
        this.url = `users/${this.auth.currentUserId}/settings`;
    }


    getSettings() {
        return this.afDatabase.object(this.url).valueChanges();
    }


    async updateGeolocation(data) {
        if (data === false) {
            // await this.geolocation.eraseGeolocation();
        }
        return this.afDatabase.object(this.url).update({geolocation: data});
    }


    updatePrelocation(data) {
        return this.afDatabase.object(this.url).update({prelocation: data});
    }

    createPrelocation(data) {
        return this.afDatabase.list(`${this.url}/prelocations`).push(data);
    }


    getPrelocations() {
        return this.afDatabase.list(`${this.url}/prelocations`, ref => ref.limitToLast(8)).valueChanges();
    }
}
